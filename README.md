## Description :

Un transfer est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :**

Ajouter un nouveau cas d'usage appelé "Deposit". Le Deposit est un dépôt d'agent cash sur un compte donné (Versement
d'argent).

Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité.

L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB.

## Assignement :

* Le code présente des anomalies de qualité (bonnes pratiques , abstraction , lisibilité ...) et des bugs .
    * localiser le maximum
    * Essayer d'améliorer la qualité du code.
    * Essayer de résoudre les bugs détectés.
* Implementer le cas d'usage `Deposit`
  Règles de gestion :
    - Le montant maximal que je peux déposer par opération est 10000DH

* Ajouter des tests unitaires.

* **Nice to have** : Ajouter une couche de sécurité

## How to use

To build the projet you will need :

* Java 17
* Maven

Build command :

```
mvn clean install
```

Run command :

```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```

## How to submit

* Fork the project into your personal gitlab space .
* Do the stuff.
* Send us the link.

## My Refactoring :

Les aspects améliorés dans le code sont : Lisibilité, Bonnes pratiques, Bugs, Implémentation du cas d'usage Deposit,
Ajout de Tests unitaires, et d'une couche de sécurité via Oauth2 client credentials

**Bonne Pratiques**

Separation of concerns :

* Anomalie :
  Implémentation de la logique métier dans la Controller TransferController
* Solution :
  Implémentation de la logique métier dans le service TransferService

* Anomalie :
  Un seul Controller TransferController pour tous les types de request
* Solution :
  Ajout de Controller UtilisateurController, CompteController... pour mieux séparer les responsabilités conformément au principe de Single Responsibility (SOLID principles).

Qualité du code et lisibilité :

* Anomalie :
  Appel de la méthode de Repository save() sur un objet récupéré avec la méthode de Repository findBy...
* Solution :
  La méthode save() attache l'objet à la session actuelle et commit les changements à la fin de la transaction, or un
  objet récupéré avec findBy est déjà attaché à la session et avec l'annotation @Transactional nous sommes sûres que les
  changements sont persistés à la fin de l'invocation de la méthode. Donc, le save() déclenche des traitements inutiles.

* Anomalie :
  Tester les objets susceptibles d'être null afin d'éviter la NullPointerException avec des if else, ce qui devient
  encombrant(boilerplate code).
* Solution :
  Utilisation de la classe Optional<T> introduite dans java 8, qui agit comme un conteneur pouvant contenir un objet ou
  null, en offrant des méthodes facilitant le traitement dans le cas ou la valeur est null comme orElseThrow() utilisée
  dans ce code.

* Amélioration :
  Ajout de la librairie Lombok qui permet de générer les getters,setters,constructeurs.. en annotant simplement la
  classe par @Getter,@Setter,@AllArgsConstructor... pour alléger le code.

* Anomalie :
  Utilisation redondante de @Autowired.

* Solution :

  D'après la documentation officielle de Spring :

  As of Spring Framework 4.3, an @Autowired annotation on such a constructor is no longer necessary if the target bean
  only defines one constructor to begin with.

  Mon code :

  J'ai utilisé l'annotation @RequiredArgsConstructor de Lombok et définit mes dépendance comme des attributs final, j'ai donc un seul
  constructeur qui injecte ces dépendances sans avoir à les annoter par @Autowired.

* Amélioration :
  Amélioration du code au niveau de la bean de gestion des exceptions annoté par @ControllerAdvice (voir code).

* Anomalie :
  La validation de TransferDto est implémenté avec des if else au niveau du Service
* Solution :
  Utilisation des Java Bean Validation Contraints du package javax.validation, qui offre des annotation(
  @NotNull,@NotEmpty, @DecimalMin...) pour appliquer des contraintes à des attributs
  Une contrainte violée lève une MethodArgumentNotValidException qui est levé au niveau du Controller et géré au
  niveau du Component annoté par @ControllerAdvice

* Anomalie :
  non respect des naming conventions pour les endpoints
* Solution:
  respect des conventions : utilisation des noms au lieu de verbes, miniscule au lieu de majuscule, separation en
  utilisant des hyphen "-"...
  
* Anomalie :
Les méthodes loadAllTransfer,loadAllUtilisateur... de controlleur retourne null si la liste retournée est vide
* Solution:
Retourner une liste vide.

* Amélioration :
utilisation des Stream en appelant la méthode stream() sur les collections afin d'effectuer des traitements sur chaque élément en chainant des opérations (map, filter...) et une opération terminale. Ceci améliore la lisibilité du code et évite des lignes de for loop

* Amélioration :
Rendre l'attribut gender de l'entité Utilisateur un enum de String (au lieu d'un String)
 
**Bugs**

* Anomalie : Comversion des BigDecimal en int pour la comparaison, perte des digits après la virgule, peut conduire par
 exemple à transférer un montant supérieur au montant permis
* Solution:
  Comparaison des BigDecimal avec la méthode compareTo()

* Anomalie :
  Le Controller n'est pas annoté avec @RequestMapping
* Solution :
  Ajout de @RequestMapping

* Anomalie :
La contrainte unique pour rib n'est pas spécifiée
* Solution:
ajout de @Column(unique = true) pour le field rib

* Anomalie :
Les repository ne sont pas annoté avec @Repository qui est une spécialisation de @Component, et ne sont donc pas reconnus comme Bean par le Spring Container.
* Solution:
Ajout de l'annotation @Repository dans les interfaces repository

* Anomalie :
Comparaison non nécessaire du montant de transfer à 0 puisque il doit être supérieur au montant minimal
* Solution:
comparer le montant à un montant minimal

* Anomalie :
la méthode auditDeposit de auditService crée un auditTransfer et l'enregistre.
* Solution :
Créer et enregistrer un auditDeposit.

**Cas d'usage Deposit**

implémentation de deposit : voir DepositService et DepositController

la contrainte "montant de deposit <= 10000" a été implémenté par l'annotation @DecimalMax de javax.validation

**Test unitaires**

Pour écrire les test unitaires les outils suivants on été utilisé :
JUnit 5 , Mockito ,AssertJ .

Le pom.xml contient déjà la dependancy "spring-boot-starter-test" qui regroupe toutes les dépendances requises pour JUnit,Mockito et AssertJ.

**Couche de sécurité**

Autoriser l'accès à l'api que pour des utilisateurs ayant un (clientId,clientSecret) oauth2 valides. LE client doit inclure le token oauth2 généré dans les headers du http request(la balise authorization).






