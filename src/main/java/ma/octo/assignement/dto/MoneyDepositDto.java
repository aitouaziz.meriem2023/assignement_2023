package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MoneyDepositDto {
    public static final String MONTANT_MAXIMAL = "10000";
    @NotNull
    private String rib;
    @NotNull
    @DecimalMax(value = MONTANT_MAXIMAL)
    private BigDecimal Montant;
    private Date dateExecution;
    @NotEmpty
    private String motifDeposit;
    @NotEmpty
    private String nom_prenom_emetteur;
}
