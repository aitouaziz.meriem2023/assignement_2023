package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TransferDto {
  public static final String MONTANT_MAXIMAL = "10000";
  public static final String MONTANT_MINIMAL = "10";

  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
  @NotEmpty
  private String motif;
  @NotNull
  @DecimalMin(value=MONTANT_MINIMAL)
  @DecimalMax(value = MONTANT_MAXIMAL)
  private BigDecimal montant;
  private Date date;
}
