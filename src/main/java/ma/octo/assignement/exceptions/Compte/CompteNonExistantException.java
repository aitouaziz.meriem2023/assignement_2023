package ma.octo.assignement.exceptions.Compte;

import ma.octo.assignement.exceptions.GeneralException;

public class CompteNonExistantException extends GeneralException {

  private static final long serialVersionUID = 1L;
  //TODO : add seperate rib not found method?

  public CompteNonExistantException(String nrCompte){
    super("CompteNonExistantException", String.format("Account with number or rib %s not found", nrCompte));
  }
  public CompteNonExistantException(Long idCompte) {
    super("CompteNonExistantException", String.format("Account with id %o  not found", idCompte));
  }
  public CompteNonExistantException() {
    super("CompteNonExistantException","Account not found" );
  }
}
