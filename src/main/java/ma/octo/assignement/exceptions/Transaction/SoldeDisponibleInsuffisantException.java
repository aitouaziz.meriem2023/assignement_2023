package ma.octo.assignement.exceptions.Transaction;

import ma.octo.assignement.exceptions.GeneralException;

public class SoldeDisponibleInsuffisantException extends GeneralException {

  private static final long serialVersionUID = 1L;
  public SoldeDisponibleInsuffisantException(String nrCompte) {
    super("SoldeDisponibleInsuffisantException",String.format("Le solde du compte numéro %s est insuffisant",nrCompte));}
  public SoldeDisponibleInsuffisantException() {
    super("SoldeDisponibleInsuffisantException","Solde insuffisant");
  }
}
