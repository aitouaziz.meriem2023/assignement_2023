package ma.octo.assignement.exceptions.Transaction;

import ma.octo.assignement.exceptions.GeneralException;

public class TransactionException extends GeneralException {

    private static final long serialVersionUID = 1L;

    public TransactionException(String message) {
        super("TransactionException",message);
    }

    public TransactionException() {
        super("TransactionException", "Transaction exception");
    }


}
