package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.dto.TransferDto;

public class EntityToDtoMapper {
    public static TransferDto transferToTransferDto(Transfer transfer) {
        return new TransferDto(transfer.getCompteEmetteur().getNrCompte(),
                transfer.getCompteBeneficiaire().getNrCompte(),
                transfer.getMotifTransfer(),
                transfer.getMontantTransfer(),
                transfer.getDateExecution());
    }

    private static MoneyDepositDto moneyDepositToMoneyDepositDto(MoneyDeposit moneyDeposit) {
        return new MoneyDepositDto(moneyDeposit.getCompteBeneficiaire().getRib(),
                moneyDeposit.getMontant(),
                moneyDeposit.getDateExecution(),
                moneyDeposit.getMotifDeposit(),
                moneyDeposit.getNom_prenom_emetteur());
    }

}
