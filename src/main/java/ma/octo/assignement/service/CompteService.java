package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CompteService implements CompteServiceInterface{
    private final CompteRepository compteRepository;
    @Override
    public List<Compte> getAllAccounts() {
        return compteRepository.findAll();
    }
}









