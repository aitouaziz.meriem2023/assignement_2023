package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface CompteServiceInterface {
    List<Compte> getAllAccounts();
}
