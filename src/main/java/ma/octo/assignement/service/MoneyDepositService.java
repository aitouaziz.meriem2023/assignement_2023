package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class MoneyDepositService implements MoneyDepositServiceInterface {
    private final CompteRepository compteRepository;
    private final MoneyDepositRepository moneyDepositRepository;
    @Override
    public void deposit(MoneyDepositDto moneyDepositDto) {
        Compte destination = compteRepository.findByRib(moneyDepositDto.getRib()).orElseThrow(() -> new CompteNonExistantException(moneyDepositDto.getRib()));

        destination.setSolde(destination.getSolde().add(moneyDepositDto.getMontant()));

        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMotifDeposit(moneyDepositDto.getMotifDeposit());
        moneyDeposit.setDateExecution(moneyDepositDto.getDateExecution());
        moneyDeposit.setCompteBeneficiaire(destination);
        moneyDeposit.setMontant(moneyDepositDto.getMontant());
        moneyDeposit.setNom_prenom_emetteur(moneyDepositDto.getNom_prenom_emetteur());

        moneyDepositRepository.save(moneyDeposit);
    }
}

