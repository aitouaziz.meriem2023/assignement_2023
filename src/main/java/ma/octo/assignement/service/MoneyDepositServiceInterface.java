package ma.octo.assignement.service;

import ma.octo.assignement.dto.MoneyDepositDto;

import java.math.BigDecimal;

public interface MoneyDepositServiceInterface {
    void deposit(MoneyDepositDto moneyDepositDto);
}
