package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transaction.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.mapper.EntityToDtoMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class TransferService implements TransferServiceInterface {
    private final CompteRepository compteRepository;
    private final TransferRepository transferRepository;


    @Override
    public List<TransferDto> getAllTransfers() {
        return transferRepository.findAll().stream().map(EntityToDtoMapper::transferToTransferDto).collect(Collectors.toList());
    }

    @Override
    public void transfer(TransferDto transferDto) {
        Compte source = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur()).orElseThrow(() -> new CompteNonExistantException(transferDto.getNrCompteEmetteur()));
        Compte destination = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire()).orElseThrow(() -> new CompteNonExistantException(transferDto.getNrCompteBeneficiaire()));
        if (source.getSolde().compareTo(transferDto.getMontant()) == -1)
            throw new SoldeDisponibleInsuffisantException(transferDto.getNrCompteEmetteur());

        source.setSolde(source.getSolde().subtract(transferDto.getMontant()));
        destination.setSolde(destination.getSolde().add(transferDto.getMontant()));

        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setCompteEmetteur(source);
        transfer.setCompteBeneficiaire(destination);
        transfer.setMotifTransfer(transferDto.getMotif());
        transfer.setDateExecution(transferDto.getDate());
        transferRepository.save(transfer);
    }

}
