package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UtilisateurService implements UtilisateurServiceInterface{
    private final UtilisateurRepository utilisateurRepository;
    @Override
    public List<Utilisateur> getAllUsers() {
        return utilisateurRepository.findAll();
    }
}
