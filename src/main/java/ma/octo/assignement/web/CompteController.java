package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/accounts")
@RequiredArgsConstructor
public class CompteController {
    private final CompteService compteService;
    @GetMapping("")
    List<Compte> loadAllCompte() {
       return compteService.getAllAccounts();
    }
}
