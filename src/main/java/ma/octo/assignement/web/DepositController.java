package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.dto.MoneyDepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transaction.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.MoneyDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/deposits")
@RequiredArgsConstructor
public class DepositController {
    Logger LOGGER = LoggerFactory.getLogger(DepositController.class);

    private final MoneyDepositService moneyDepositService;
    private final AuditService auditService;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody @Valid MoneyDepositDto moneyDepositDto)
            throws  CompteNonExistantException, MethodArgumentNotValidException {
        moneyDepositService.deposit(moneyDepositDto);
        auditService.auditDeposit("Deposit par " + moneyDepositDto.getNom_prenom_emetteur() + " au profit du compte de rib " + moneyDepositDto
                .getRib() + " d'un montant de " + moneyDepositDto.getMontant()
                .toString());
    }

}
