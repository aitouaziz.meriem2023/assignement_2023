package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transaction.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/transfers")
@RequiredArgsConstructor
class TransferController {
    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final TransferService transferService;
    private final AuditService auditService;

    @GetMapping("")
    List<TransferDto> loadAllTransfers() {
        LOGGER.info("Lister les transfers");
        return transferService.getAllTransfers();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody @Valid TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException , MethodArgumentNotValidException {
        transferService.transfer(transferDto);
        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }

}
