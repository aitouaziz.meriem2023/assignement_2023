package ma.octo.assignement.web.common;

import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transaction.SoldeDisponibleInsuffisantException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class ExceptionHandelingController {
    Logger LOGGER = LoggerFactory.getLogger(ExceptionHandelingController.class);

    @ExceptionHandler({
            SoldeDisponibleInsuffisantException.class,
            CompteNonExistantException.class,
            MethodArgumentNotValidException.class
    })
    @Nullable
    public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();

        LOGGER.error(("Handling " + ex.getClass().getSimpleName() + " due to " + ex.getMessage()));

        if (ex instanceof SoldeDisponibleInsuffisantException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            SoldeDisponibleInsuffisantException soldeDisponibleInsuffisantException
                    = (SoldeDisponibleInsuffisantException) ex;

            return handleSoldeDisponibleInsuffisantException(soldeDisponibleInsuffisantException, headers, status, request);
        } else if (ex instanceof CompteNonExistantException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            CompteNonExistantException compteNonExistantException = (CompteNonExistantException) ex;

            return handleCompteNonExistantException(compteNonExistantException, headers, status, request);
        } else if (ex instanceof MethodArgumentNotValidException) {
            HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;
            MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) ex;

            return handleConstraintViolation(methodArgumentNotValidException, headers, status, request);
        } else {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Unknown exception type: " + ex.getClass().getName());
            }
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return handleExceptionInternal(ex, null, headers, status, request);
        }
    }


    public ResponseEntity<ApiError> handleSoldeDisponibleInsuffisantException(SoldeDisponibleInsuffisantException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = Collections.singletonList(ex.getMessage());
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }


    public ResponseEntity<ApiError> handleCompteNonExistantException(CompteNonExistantException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = Collections.singletonList(ex.getMessage());
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }

    public ResponseEntity<ApiError>
    handleConstraintViolation(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.info("Constraint violation exception encountered: {}", ex.getMessage());
        final List<String> errors = new ArrayList<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, @Nullable ApiError body,
                                                               HttpHeaders headers, HttpStatus status,
                                                               WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        return new ResponseEntity<>(body, headers, status);
    }
}
