package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AuditServiceTest {
    @InjectMocks
    AuditService auditService;

    @Mock
    AuditTransferRepository auditTransferRepository;
    @Mock
    AuditDepositRepository auditDepositRepository;
    @Captor
    ArgumentCaptor<AuditTransfer> auditTransferArgumentCaptor;
    @Captor
    ArgumentCaptor<AuditDeposit> auditDepositArgumentCaptor;

    @Test
    @DisplayName("Should create audit transfer")
    void shouldCreateAuditTransfer() {
        //given
        String msg = "Audit transfer message";
        //when
        auditService.auditTransfer(msg);
        //then
        verify(auditTransferRepository, times(1)).save(auditTransferArgumentCaptor.capture());
        assertEquals(auditTransferArgumentCaptor.getValue().getMessage(),msg);
    }

    @Test
    @DisplayName("Should create audit deposit")
    void shouldCreateAuditDeposit() {
        //given
        String msg = "Audit deposit message";
        //when
        auditService.auditDeposit(msg);
        //then
        verify(auditDepositRepository, times(1)).save(auditDepositArgumentCaptor.capture());
        assertEquals(auditDepositArgumentCaptor.getValue().getMessage(),msg);
    }
}