package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CompteServiceTest {
    @InjectMocks
    CompteService compteService;
    @Mock
    CompteRepository compteRepository;

    @Test
    @DisplayName("Should get all accounts")
    void shouldGetAllAccounts() {
        //given
        Compte compte1 = new Compte(1L, "1A", "123", new BigDecimal(10), new Utilisateur());
        Compte compte2 = new Compte(2L, "1B", "1234", new BigDecimal(10), new Utilisateur());
        Mockito.when(compteRepository.findAll()).thenReturn(List.of(compte1, compte2));
        //when
        List<Compte> comptes = compteService.getAllAccounts();
        //then
        Mockito.verify(compteRepository, Mockito.times(1)).findAll();
        assertEquals(2, comptes.size());
    }
}