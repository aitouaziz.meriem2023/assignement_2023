package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transaction.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {
    @InjectMocks
    TransferService transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    CompteRepository compteRepository;
    @Captor
    ArgumentCaptor<Transfer> transferArgumentCaptor;


    @Test
    @DisplayName("Should get all transfers")
    void shouldGetAllTransfers() {
        //given
        Transfer t1 = new Transfer(1L,new BigDecimal(10),new Date(),new Compte(),new Compte(),"motif1");
        when(transferRepository.findAll()).thenReturn(List.of(t1));
        //when
        List<TransferDto> transfers= transferService.getAllTransfers();
        //then
        verify(transferRepository,times(1)).findAll();
        assertEquals(1,transfers.size());
    }


    @Test
    @DisplayName("Should throw exception when account not found")
    void shouldThrowExceptionWhenAccountNotFound() {
        //given
        TransferDto transferDto = new TransferDto("1Z","1K","motif",new BigDecimal(0),new Date());
        when(compteRepository.findByNrCompte(Mockito.anyString())).thenReturn(Optional.empty());
        //then
        assertThrows(CompteNonExistantException.class,()->{transferService.transfer(transferDto);});
    }
    @Test
    @DisplayName("Should throw exception when account balance is insufficient for transfer")
    void shouldThrowExceptionWhenInsufficientBalance(){
        //given
        Compte compte1 = new Compte(1L,"1A","123",new BigDecimal(10),new Utilisateur());
        Compte compte2 = new Compte(2L,"1B","1234",new BigDecimal(10),new Utilisateur());
        TransferDto transferDto = new TransferDto("1A","1B","motif",new BigDecimal(15),new Date());
        when(compteRepository.findByNrCompte("1A")).thenReturn(Optional.of(compte1));
        when(compteRepository.findByNrCompte("1B")).thenReturn(Optional.of(compte2));
        //then
        assertThrows(SoldeDisponibleInsuffisantException.class,()->transferService.transfer(transferDto));
    }
    @Test
    @DisplayName("Should execute transfer")
    void shouldExecuteTransfer(){
        //given
        Compte compte1 = new Compte(1L,"1A","123",new BigDecimal(10),new Utilisateur());
        Compte compte2 = new Compte(2L,"1B","1234",new BigDecimal(10),new Utilisateur());
        TransferDto transferDto = new TransferDto("1A","1B","motif",new BigDecimal(5),new Date());
        when(compteRepository.findByNrCompte("1A")).thenReturn(Optional.of(compte1));
        when(compteRepository.findByNrCompte("1B")).thenReturn(Optional.of(compte2));
        //when
        transferService.transfer(transferDto);
        //then
        assertEquals(compte1.getSolde(),new BigDecimal(5));
        assertEquals(compte2.getSolde(),new BigDecimal(15));
        verify(transferRepository,times(1)).save(transferArgumentCaptor.capture());
        assertEquals(transferArgumentCaptor.getValue().getMontantTransfer(),transferDto.getMontant());
    }


}