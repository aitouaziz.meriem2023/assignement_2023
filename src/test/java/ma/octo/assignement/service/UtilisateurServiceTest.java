package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UtilisateurServiceTest {
    @InjectMocks
    UtilisateurService utilisateurService;

    @Mock
    UtilisateurRepository utilisateurRepository;

    @Test
    @DisplayName("should get all users")
    void getAllUsers() {
        //given
        Utilisateur utilisateur = new Utilisateur(1L,"username", Gender.MALE,"lastname","firstname",new Date());
        Mockito.when(utilisateurRepository.findAll()).thenReturn(List.of(utilisateur));
        //when
        List<Utilisateur> utilisateurs= utilisateurService.getAllUsers();
        //then
        Mockito.verify(utilisateurRepository,Mockito.times(1)).findAll();
        assertEquals(1,utilisateurs.size());
    }
}